require 'ruby-debug'
require 'fast_stemmer'

Debugger.settings[:autoeval] = true

ARGF.each do |line|
  line.split.each do |word|
    puts word.stem
  end
end
