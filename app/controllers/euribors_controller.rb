class EuriborsController < ApplicationController
  # GET /euribors
  # GET /euribors.json
  def index
    @euribors = Euribor.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @euribors }
    end
  end

  # GET /euribors/1
  # GET /euribors/1.json
  def show
    @euribor = Euribor.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @euribor }
    end
  end

  # GET /euribors/new
  # GET /euribors/new.json
  def new
    @euribor = Euribor.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @euribor }
    end
  end

  # GET /euribors/1/edit
  def edit
    @euribor = Euribor.find(params[:id])
  end

  # POST /euribors
  # POST /euribors.json
  def create
    @euribor = Euribor.new(params[:euribor])

    respond_to do |format|
      if @euribor.save
        format.html { redirect_to @euribor, notice: 'Euribor was successfully created.' }
        format.json { render json: @euribor, status: :created, location: @euribor }
      else
        format.html { render action: "new" }
        format.json { render json: @euribor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /euribors/1
  # PUT /euribors/1.json
  def update
    @euribor = Euribor.find(params[:id])

    respond_to do |format|
      if @euribor.update_attributes(params[:euribor])
        format.html { redirect_to @euribor, notice: 'Euribor was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @euribor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /euribors/1
  # DELETE /euribors/1.json
  def destroy
    @euribor = Euribor.find(params[:id])
    @euribor.destroy

    respond_to do |format|
      format.html { redirect_to euribors_url }
      format.json { head :ok }
    end
  end
end
