class MakeLinkUniqueInStories < ActiveRecord::Migration
  def change
    add_index :stories, :link, :unique => true
  end
end
