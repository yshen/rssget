class CreateEuribors < ActiveRecord::Migration
  def change
    create_table :euribors do |t|
      t.date :date
      t.float :w1
      t.float :w2
      t.float :w3
      t.float :m1
      t.float :m2
      t.float :m3
      t.float :m4
      t.float :m5
      t.float :m6
      t.float :m7
      t.float :m8
      t.float :m9
      t.float :m10
      t.float :m11
      t.float :m12

      t.timestamps
    end
  end
end
