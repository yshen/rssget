class AddCorruptToStories < ActiveRecord::Migration
  def change
    add_column :stories, :corrupt, :boolean
  end
end
