class AddDateUniquenessToEuribors < ActiveRecord::Migration
  def change
    add_index :euribors, :date, :unique => true
  end
end
