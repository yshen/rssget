class AddRawContentToStories < ActiveRecord::Migration
  def change
    add_column :stories, :raw_content, :text
  end
end
