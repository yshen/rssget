class AddIndexToDoc < ActiveRecord::Migration
  def change
    change_column :docs, :doc_id, :int, { :unique => true }
  end
end
