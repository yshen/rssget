class CreateDocs < ActiveRecord::Migration
  def change
    create_table :docs do |t|
      t.integer :doc_id
      t.integer :real_doc_id

      t.timestamps
    end
  end
end
