class AddIsHistoricalToStories < ActiveRecord::Migration
  def change
    add_column :stories, :is_hist, :boolean, :default => false
  end
end
