class AddDateTimeToStories < ActiveRecord::Migration
  def change
    add_column :stories, :date_time, :datetime
  end
end
