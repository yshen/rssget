class AddTermIdToTerms < ActiveRecord::Migration
  def change
    add_column :terms, :term_id, :int
  end
end
