# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120305211343) do

  create_table "docs", :force => true do |t|
    t.integer  "doc_id"
    t.integer  "real_doc_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "euribors", :force => true do |t|
    t.date     "date"
    t.float    "w1"
    t.float    "w2"
    t.float    "w3"
    t.float    "m1"
    t.float    "m2"
    t.float    "m3"
    t.float    "m4"
    t.float    "m5"
    t.float    "m6"
    t.float    "m7"
    t.float    "m8"
    t.float    "m9"
    t.float    "m10"
    t.float    "m11"
    t.float    "m12"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "euribors", ["date"], :name => "index_euribors_on_date", :unique => true

  create_table "stories", :force => true do |t|
    t.string   "title"
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "date_time"
    t.string   "topic"
    t.string   "source"
    t.text     "content"
    t.text     "raw_content"
    t.boolean  "is_hist",     :default => false
    t.boolean  "corrupt"
  end

  add_index "stories", ["link"], :name => "index_stories_on_link", :unique => true

  create_table "storiesBackup", :force => true do |t|
    t.string   "title"
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "date_time"
    t.string   "topic"
    t.string   "source"
    t.text     "content"
    t.text     "raw_content"
    t.boolean  "is_hist",     :default => false
  end

  add_index "storiesBackup", ["link"], :name => "index_stories_on_link", :unique => true

  create_table "tdm", :id => false, :force => true do |t|
    t.integer "doc"
    t.integer "term"
    t.integer "freq"
  end

  create_table "terms", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "term_id"
  end

  create_table "view1", :id => false, :force => true do |t|
    t.integer "id_euribors", :default => 0, :null => false
  end

end
