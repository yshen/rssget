Gem::Specification.new do |s|
  s.name = %q{lltf}
  s.version = "0.0.1"
  s.authors = ["Yu Shen"]
  s.date = %q{2012-03-13}
  s.description = %q{For logistic regression on tf matrices}
  s.email = ["yushen83@gmail.com"]
  s.has_rdoc = true
  s.summary = %q{Logistic Line-fitting Term Frequency}
  s.rubyforge_project = %q{lltf}
  s.rubygems_version = %q{0.0.1}
  s.files = ["README.txt"]
end
