require 'csv'
require 'ruby-debug'
require 'date'

class EuriborLoader
  def self.load_csv(str)
    arr=[]
    x = CSV.parse(str) do |a|
      arr << a
    end
    n=arr[0].size
    (1...n).each {|i|
    begin
      date=Date.parse(arr[0][i])
      euribor=Euribor.new
      euribor.date=date
      euribor.w1=arr[1][i]
      euribor.w2=arr[2][i]
      euribor.w3=arr[3][i]
      euribor.m1=arr[4][i]
      euribor.m2=arr[5][i]
      euribor.m3=arr[6][i]
      euribor.m4=arr[7][i]
      euribor.m5=arr[8][i]
      euribor.m6=arr[9][i]
      euribor.m7=arr[10][i]
      euribor.m8=arr[11][i]
      euribor.m9=arr[12][i]
      euribor.m10=arr[13][i]
      euribor.m11=arr[14][i]
      euribor.m12=arr[15][i]
      euribor.save
    rescue => ex
      puts "#{ex.class}: #{ex.message}"
    end
    }
    #arr_of_rows.each {|x| puts x.class}

    arr_of_rows=[]
    t = CSV::Table.new(arr_of_rows)
    return t
  end
  
  def self.main(filename)
    f = open(filename)
    str=f.read
    t = self.load_csv(str)
    #t.by_col!
    #t.each {|x| print x}
  end
    
end

if __FILE__ == $PROGRAM_NAME
  EuriborLoader.main ARGV[0]
end

