require 'rss/1.0'
require 'rss/2.0'

class StoryParser
def self.parse(content)
  rss = RSS::Parser::parse(content, false)
  for item in rss.items
    story = self.create_story(item)
    puts "#{self.story_2_string(story)}"
  end
  return rss
puts "Root values"
print "RSS title: ", rss.channel.title, "\n"
print "RSS link: ", rss.channel.link, "\n"
print "RSS description: ", rss.channel.description, "\n"
print "RSS publication date: ", rss.channel.date, "\n"

puts "Item values"
print "number of items: ", rss.items.size, "\n"
print "title of first item: ", rss.items[0].title, "\n"
print "link of first item: ", rss.items[0].link, "\n"
#print "description of first item: ", rss.items[0].description, "\n"
print "date of first item: ", rss.items[0].date, "\n"


end

def self.create_story(item)
  return "#{item.title},#{item.date},#{item.source},#{item.link}"
end

def self.story_2_string(story)
  return story
end
end

