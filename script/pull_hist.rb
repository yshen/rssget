require 'open-uri'
require 'nokogiri'
require 'ruby-debug'
require 'cgi'
Debugger.settings[:autoeval] = true
require './script/pull.rb'
require 'rss/1.0'
require 'rss/2.0'

class PullHist
def self.create_story_from_index(idx)
  story = Story.new
  story.is_hist = true
  rawlink = @links[idx]
  params = CGI::parse(rawlink["href"])
  story.link = params["url"][0]

  story.title=@titles[idx].children.text
      
  matchdata = story.link.match'https?://(?:w{3}\.)?(.*?)/.*' #Since <source> tag is rarely set in Google News, we use domain name to identify source, e.g. www.nytimes.com, but still, only http(s) is going to work

  story.source=matchdata[1]
  matchdata = @topics[idx].children.text.match '(.*) - Google News'
  story.topic = matchdata[1]

  date_text = @dates[idx].children.text
  if date_text.match '[AP]M'

    story.date_time = Date.new(ARGV[1].to_i, ARGV[2].to_i, ARGV[3].to_i)
  else
    story.date_time = Date.parse(date_text)
  end

  puts "Title: "+story.title
  puts "Link: "+story.link
  puts "Source: "+story.source
  puts "Date: "+story.date_time.to_s

 
#    raw_text = self.read_story(story.link)

    if !(Pull::SOURCES_CSS.keys.include? story.source)
      puts "Skipping text extraction from non-trusted source "+story.source+":"+story.title
#      story.raw_content = open(story.link) do |s| Iconv.conv('UTF-8','ISO_8859-1',s.read) end
    else
      story.content = Iconv.conv('UTF-8','ISO_8859-1', Pull.extract_text(story.link, Pull::SOURCES_CSS[story.source]))
      if story.content == nil
        puts "Text extraction failed"
      end
    end
  begin
    story.save
    puts "Saved "+story.title
  rescue SQLite3::ConstraintException,ActiveRecord::RecordNotUnique => ex
    puts "Skipped duplicated story: "+story.title
  end
      
#doc.css 'div[class="storyText"]' cbsnews.com
#'span[id="articleText"]' reuters
  return story

end

def self.main
@today = Date.today
link = ARGV[0]
doc = Nokogiri::HTML(open(link))
@snippets = doc.css('span[class="snippet"]')
@titles = doc.css('h2[class="entry-title"]')
@links = doc.css('a[class="entry-original"]')
@dates = doc.css('div[class="entry-date"]')
@topics = doc.css('span[class="entry-source-title"]')
n = @titles.length
for i in 0..(n-1)
  story = self.create_story_from_index(i)
end
end
end


if __FILE__ == $PROGRAM_NAME
    # Put "main" code here
#    raise 'test'
    PullHist.main
end
