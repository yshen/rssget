require 'test_helper'

class EuriborsControllerTest < ActionController::TestCase
  setup do
    @euribor = euribors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:euribors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create euribor" do
    assert_difference('Euribor.count') do
      post :create, euribor: @euribor.attributes
    end

    assert_redirected_to euribor_path(assigns(:euribor))
  end

  test "should show euribor" do
    get :show, id: @euribor.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @euribor.to_param
    assert_response :success
  end

  test "should update euribor" do
    put :update, id: @euribor.to_param, euribor: @euribor.attributes
    assert_redirected_to euribor_path(assigns(:euribor))
  end

  test "should destroy euribor" do
    assert_difference('Euribor.count', -1) do
      delete :destroy, id: @euribor.to_param
    end

    assert_redirected_to euribors_path
  end
end
